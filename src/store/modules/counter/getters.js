export default {
  //finalCounter(state, getters) {} - this is commented out just to show that
  // when you build getters you always can use the state and other getters inside of a getter,
  // but you don't have to use the getters if you don't want to.
  finalCounter(state) {
    return state.counter;
  }, // THIS IS A WAY of how to do that if there is need. You can define rootState as arugment to the // getter function. You don't have to use them all, you can omit them by using _ _1 _2 etc, but it's //good to be aware of the option.
  /* IMPORTANT: Now that we moved counter into a local module, there is a thing. This whole thing is LOCAL
            and you can't just read the state of isAuth like accessing the counter state.
            So writing some getter like:
            readAuth(state) { return state.isAuth}
            won't work. isAuth is in the store, it's not in this module, therefore we can't just access it
            like that. this also applies to actions. You can't just make a mutation or action for a state
            in the store from a local module like this. */
  //RIGHT WAY TO DO IT:
  /* testAuth(state,getters,rootState, rootGetters) {
          return state.isAuth;
        }, 
        */ normalizedCounter(
    _,
    getters
  ) {
    // Ah the _ is the one from the previous lectures, where you signal
    // the IDE that you are getting the first argument, so you get to the second one, but you don't use it below so it won't complain about it.
    const finalCounter = getters.finalCounter;
    if (finalCounter < 0) {
      return 0;
    }
    if (finalCounter > 100) {
      return 100;
    }
    return finalCounter;
  }
};
