import counterGetters from './getters.js';
import counterActions from './actions.js';
import counterMutations from './mutations.js';

/* If we want to better manage the store code, we should make the global store code leaner, and work with
smaller parts, similar to components. Especially for store stuff that is not used globally everywhere.
This is where Modules comes into play. Module is at the same level as the global store. It's exactly the
 same as defining it inside the store, but this way we can outsource some of the code, and define it outside,
 making the global store code leaner and more manageable. Below is how we define a module.
Basically make a js object with name of your choice, and add the same stuff as to the regular store: 
state, mutations, actions, getters. Continues on store defining part.*/
export default {
  namespaced: true,
  state() {
    return {
      counter: 0
    };
  },
  mutations: counterMutations,
  actions: counterActions,
  getters: counterGetters
};

//Continues from the module part: Next is here, we tell the global store that we have modules as well
// defined around somewhere. We add another key:value pair called modules: { nameOfModule: actualJSobjectFromAbove} to the global store.
