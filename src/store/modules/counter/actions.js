export default {
  //you can use same names between mutations, getters and actions
  increment(context) {
    setTimeout(function() {
      context.commit('increment');
    }, 500);
  },
  increase(context, payload) {
    console.log(context);
    context.commit('increase', payload);
  }
};
