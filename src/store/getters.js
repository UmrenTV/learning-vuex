export default {
  isAuth(state) {
    return state.authenticated;
  }
};
