export default {
  login(context) {
    /* context.commit('login'); */
    context.commit('setAuth', { isAuth: true });
  },
  logout(context) {
    /* context.commit('logout'); */
    context.commit('setAuth', { isAuth: false });
  }
};
