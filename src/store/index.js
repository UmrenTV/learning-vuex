import { createStore } from 'vuex';
import rootActions from './actions.js';
import rootGetters from './getters.js';
import rootMutations from './mutations.js';
import counterModule from './modules/counter/index.js';

const store = createStore({
  modules: {
    numbers: counterModule
  },
  state() {
    return {
      authenticated: false
    };
  },
  mutations: rootMutations,
  getters: rootGetters,

  actions: rootActions
});

export default store;
